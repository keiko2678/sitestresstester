# SiteStressTester #

Console application written in C# that aims to test the average responses a web-server is capable of answering on per second. It maps a certain amount of sub-pages of the requested domain first, and then asks them randomly. 

The amount of threads used to ask for page-requests will grow automatically up to a defined limit until it finds it is not effective to add any more threads.


## Version 0.1 screenshot ##
![Screenshot1.png](https://bitbucket.org/repo/6an5zE/images/1674854491-Screenshot1.png)