﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SiteStressTester
{
    class SiteMapper
    {
        private readonly WebPage _basePage;
        public Uri BaseUri;

        public HashSet<Uri> SiteUrls;
        private readonly HashSet<Uri> _toCheck;
        public HashSet<Uri> Checked;

        private readonly List<Thread> _threads;
        private readonly Thread _drawerThread;

        private double _totalDownload;
        private readonly DateTime _startTime;
        private int _totalRequests;
        private readonly int _maxPagesToMap;

        public SiteMapper(Uri baseUri, int maxPagesToMap = Program.MaxPagesToMap, int numberOfThreads = 8)  
        {
            BaseUri = baseUri;
            _maxPagesToMap = maxPagesToMap;
            _basePage = new WebPage(baseUri, BaseUri);
            SiteUrls = new HashSet<Uri>();
            _toCheck = new HashSet<Uri>();
            Checked = new HashSet<Uri>();
            _basePage.InternalLinks.Do(l => _toCheck.Add(l));
            _threads = new List<Thread>();

            if (!_basePage.Exists)
            {
                Console.Write("\nBaseURL not valid.");
                return;
            }

            SiteUrls.Add(BaseUri);
            _startTime = LocalTime.Now;

            Console.Write("\n\n\n\n");
            _drawerThread = new Thread(() => Drawer());
            _drawerThread.Start();
            for (var i = 1; i <= numberOfThreads; i++)
            {
                var thread = new Thread(() => Mapper());
                lock (_threads)
                {
                    _threads.Add(thread);
                }
                thread.Start();
            }

            foreach (var thread in _threads)
            {
                thread.Join();
            }
            _drawerThread.Join();
        }

        private void Mapper()
        {
            Uri uri = null;
            WebPage webPage;
          
            while (true)
            {
                lock (_toCheck)
                {
                    do
                    {
                        if (_toCheck.None())
                            return;
                        uri = _toCheck.First();
                        _toCheck.Remove(uri);
                    } while (Checked.Contains(uri));
                }

                webPage = new WebPage(uri, BaseUri);

                lock (Checked)
                {
                    if (_maxPagesToMap > 0 && Checked.Count() >= _maxPagesToMap)
                        return;

                    Checked.Add(uri);
                }

                lock (_toCheck)
                {
                    _toCheck.AddRange(webPage.InternalLinks.Except(Checked));
                }

                _totalRequests += webPage.TotalRequestsDone();
                _totalDownload += webPage.TotalSizeInByte();

                lock (SiteUrls)
                {
                    SiteUrls.AddRange(webPage.InternalLinks);
                }
               
            }
        }

        private void Drawer()
        {
            var count = 0;
            var pagesToCheck = 0;
            var isChecked = 0;
            while (true)
            { 
                lock (SiteUrls)
                {
                    count = SiteUrls.Count();
                }
                lock (_toCheck)
                {
                    pagesToCheck = _toCheck.Count();
                }
                lock (Checked)
                {
                    isChecked = Checked.Count();
                }
                Thread.Sleep(100);

                StatusDrawer(count, pagesToCheck, isChecked);

                if (pagesToCheck == 0 || (_maxPagesToMap > 0 && Checked.Count() >= _maxPagesToMap))
                    break;
            }
            StatusDrawer(SiteUrls.Count(), 0, Checked.Count(), true);
        }

        private void StatusDrawer(int count, int toCheck, int isChecked, bool endstats = false)
        {
            var currentTime = LocalTime.Now.Subtract(_startTime);
            var bitPerSec = _totalDownload / currentTime.TotalSeconds;
            var requestsPerSec = _totalRequests / currentTime.TotalSeconds;
            var speed = "";
            var downloaded = "";
            var requests = "";
            var pagesToCheck = "";
            var status = "Starting";
            var pagesCheked ="";
            var numberOfThreads = "";
            int threadCount;
            var time = "";

            lock (_threads)
            {
                threadCount=_threads.Count();
            }

            if (_totalDownload > 0 && !endstats)
            {
                speed =         "Speed: " + WebPage.GetTotalSizeHumanReadable(bitPerSec) + "\\sec";
                downloaded =    " - Total: " + WebPage.GetTotalSizeHumanReadable(_totalDownload);
                requests =      " - " + string.Format("{0:0.0}", requestsPerSec) + " requests\\sec";
                pagesToCheck =  " - Pages to check: " + toCheck.ToString("N0");
                status = "Mapping";
                numberOfThreads = " - On " + threadCount + " threads";
                time = " - runningtime " + LocalTime.Now.Subtract(_startTime).ToString(@"hh\:mm\:ss");

            }
            pagesCheked = "Pages cheked: " + isChecked;

            if (endstats)
                status = "Site mapped";

            Console.CursorTop -= 4;
            Program.ClearConsoleLine(5);
            Console.CursorLeft = 0;
            Console.Write("  " + BaseUri.Host + BaseUri.AbsolutePath.TrimEnd("/")+ " - " + status
                + "\n\n  Current uniqe subsites found: " + count.ToString("N0") + pagesToCheck
                + "\n  " + pagesCheked + numberOfThreads + time
                + "\n  "+ speed + requests + downloaded);

            if (endstats)
                Console.Write("\n");
        }

        
    }
}
