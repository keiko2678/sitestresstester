﻿using System;
using System.Reflection;
using System.Diagnostics;

namespace SiteStressTester
{
	class Program
	{
        public const int MaxThreadsOnStress = 100;
        public const int SecondsOffStressing = 30;
        public const int MaxPagesToMap = 100;

        public static void Main(string[] args)
		{
            ShowCredits();

			
            var uri = new Uri(@"https://www.keikoinnovation.com/");
           // var uri = new Uri(@"https://www.nrk.no");
            new StressTester(uri, SecondsOffStressing.Seconds());
            //new SingleUrlTester(4,uri,750);

            KeyToExit();
		}

        private static void KeyToExit()
        {
            while (Console.KeyAvailable)
            {
                Console.ReadKey(intercept: true);
            }
            Console.Write("\n\n *** Press any key to exit ***");
            Console.ReadKey();
        }

        public static void ClearConsoleLine(int count = 1)
        {
            for (var i = 1; i <= count; i++)
            {
                var currentLineCursor = Console.CursorTop;
                Console.SetCursorPosition(0, Console.CursorTop);
                Console.Write(new string(' ', Console.WindowWidth));
                Console.SetCursorPosition(0, currentLineCursor);
                Console.Write("\n");
            }
            Console.CursorTop -= count;
            Console.CursorLeft = 0;
        }

        private static void ShowCredits()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            var version = fvi.FileVersion;


            Console.Write("\n  **************************************" +
                          "\n  *                                    *" +
                          "\n  *  SiteStressTester                  *" +
                          "\n  *                                    *" +
                                  "\n  *  Version: " + version + new string(' ', 25 - version.Length) + "*" +
                          "\n  *  By: P.C. Kofstad                  *" +
                          "\n  *                                    *" +
                          "\n  **************************************\n\n");
        }
    }
}
