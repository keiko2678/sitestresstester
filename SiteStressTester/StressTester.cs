﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SiteStressTester
{
    internal class StressTester
    {
        public Uri BaseUri;

        public HashSet<Uri> PagesToStress;

        private double _totalDownload;
        private readonly DateTime _startTime;
        private int _totalRequests;
        private readonly DateTime _endTime;

        private readonly double _lastThreadMax;
        private double _maxRequestPerSec;
        private readonly int _threadCount = 1;

        public StressTester(Uri baseUri, TimeSpan lengtOfTest, int maxThreads = Program.MaxThreadsOnStress)
        {
            BaseUri = baseUri;
            PagesToStress = new SiteMapper(baseUri).Checked ?? new HashSet<Uri>();

            if (PagesToStress.None())
                return;

            var threads = new List<Thread>();

            Console.Write("\n\n\n\n");
            var drawerThread = new Thread(() => Drawer());

            _startTime = LocalTime.Now;
            _endTime = _startTime.Add(lengtOfTest);
            drawerThread.Start();

            while (true)
            {
                if (LocalTime.Now.IsAfterOrEqualTo(_endTime.AddSeconds(-2)))
                    break;

                if (_lastThreadMax < _maxRequestPerSec || _threadCount < 4)
                {
                    _lastThreadMax = _maxRequestPerSec;
                    var thread = new Thread(Mapper);
                    lock (threads)
                    {
                        _threadCount = threads.Count();
                        if (_threadCount >= maxThreads)
                            break;
                        threads.Add(thread);
                    }
                    thread.Start();
                }
                if (_threadCount >= 4)
                    Thread.Sleep(500);
            }

            foreach (var thread in threads)
            {
                thread.Join();
            }
            drawerThread.Join();
        }

        private void Mapper()
        {
            while (true)
            {
                Uri uri;
                lock (PagesToStress)
                {
                    uri = PagesToStress.PickRandom();
                }

                var webPage = new WebPage(uri, BaseUri);

                _totalRequests += webPage.TotalRequestsDone();
                _totalDownload += webPage.TotalSizeInByte();

                if (LocalTime.Now.IsAfter(_endTime))
                    break;
            }
        }

        private void Drawer()
        {
            while (true)
            {
                Thread.Sleep(100);

                StatusDrawer();

                if (LocalTime.Now.IsAfter(_endTime))
                    break;
            }
            StatusDrawer(true);
        }

        private void StatusDrawer(bool endstats = false)
        {
            var currentTime = LocalTime.Now.Subtract(_startTime);
            var bitPerSec = _totalDownload / currentTime.TotalSeconds;
            var lastRequestPerSec = _totalRequests / currentTime.TotalSeconds;

            if (lastRequestPerSec > _maxRequestPerSec)
                _maxRequestPerSec = lastRequestPerSec;

            var speed = "";
            var downloaded = "";
            var requests = "";
            var currentMaxRequests = "";
            var status = "Stressing";
            var numberOfThreads = "";
            var time = "";

            if (_totalDownload > 0)
            {
                speed = "Speed: " + WebPage.GetTotalSizeHumanReadable(bitPerSec) + "\\sec";
                downloaded = " - Total downloaded: " + WebPage.GetTotalSizeHumanReadable(_totalDownload);
                requests = " - " + $"{lastRequestPerSec:0.0}" + " requests\\sec";
                currentMaxRequests = "Current max registered: " + string.Format("{0:0.0}", _maxRequestPerSec) + " requests\\sec";
                numberOfThreads = " - On " + _threadCount + " threads";
                time = " - runningtime " + LocalTime.Now.Subtract(_startTime).ToString(@"hh\:mm\:ss");

            }
            var totalRequests = "Total requests: " + _totalRequests;

            if (endstats)
                status = "Test finnished";

            Console.CursorTop -= 4;
            Program.ClearConsoleLine(5);
            Console.CursorLeft = 0;
            Console.Write("  " + status
                + "\n  " + totalRequests + downloaded + time
                + "\n  " + speed + requests + numberOfThreads
                + "\n\n  " + currentMaxRequests);

            if (endstats)
                Console.Write("\n");
        }
    }
}
