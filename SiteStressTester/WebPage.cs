﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SiteStressTester
{
	internal class WebPage
	{
		private readonly Uri _baseUri;
        public Uri Uri;
        private readonly string _content;
		public Dictionary<Uri, string> Scriptsfiles;
		public Dictionary<Uri, string> Stylefiles;
		public Dictionary<Uri, byte[]> Pictures;
		private readonly HashSet<Uri> _links;
        public IEnumerable<Uri> InternalLinks
        {
            get
            {
                return _links.Where(l=> IsPartOfSite(l)).ToArray();
            }
        }
        public IEnumerable<Uri> PublicLinks
        {
            get
            {
                return _links.Where(l => !IsPartOfSite(l)).ToArray();
            }
        }
        internal TimeSpan LoadTime;
        public bool Exists;

		internal WebPage(Uri uri, Uri baseUri)
		{
            Uri = uri;
			_baseUri = baseUri;
			Scriptsfiles = new Dictionary<Uri, string>();
			Stylefiles = new Dictionary<Uri, string>();
			Pictures = new Dictionary<Uri, byte[]>();
			_links = new HashSet<Uri>();

            var doc = new HtmlDocument();
            try
            {
                doc = new HtmlWeb().Load(Uri.ToString());
                Exists = true;
            }

            catch
            {
                Exists = false;
            }

            _content = doc.DocumentNode.ToString();

            // links
            var linkNodes = doc.DocumentNode.Descendants("a");
            foreach (var node in linkNodes)
            {
                var url = GetUri(node.Attributes["href"]?.Value.TrimAfter("?"));
                if (url != null && url.ToString().Lacks("javascript:"))
                    _links.Add(url);
            }
        }

		internal async Task LoadAll()
		{
			var startime = LocalTime.Now;
            var doc = new HtmlDocument();
            doc.LoadHtml(_content);

            // style
            var styleNodes = doc.DocumentNode.Descendants("link");
			foreach (var node in styleNodes)
			{
				if (node.Attributes["rel"]?.Value == "stylesheet")
				{
					var url = GetUri(node.Attributes["href"]?.Value);
					if (url != null && Stylefiles.LacksKey(url))
						Stylefiles.Add(url, await GetStringFile(url));
				}
			}

			// script
			var scriptNodes = doc.DocumentNode.Descendants("script");
			foreach (var node in scriptNodes)
			{
				var url = GetUri(node.Attributes["src"]?.Value);
				if (url != null && Scriptsfiles.LacksKey(url))
				 Scriptsfiles.Add(url, await GetStringFile(url));
			}

			// pictures
			var pictureNodes = doc.DocumentNode.Descendants("img");
			foreach (var node in pictureNodes)
			{
				var url = GetUri(node.Attributes["src"]?.Value);
				if (url != null && Pictures.LacksKey(url))
					Pictures.Add(url, await GetByteFile(url));
			}

			LoadTime = LocalTime.Now.Subtract(startime);
		}

        internal int TotalRequestsDone()
        {
            if (_content == null)
                return 0;

            var result = 1;

            result += Scriptsfiles.Count();
            result += Stylefiles.Count();
            result += Pictures.Count();

            return result;
        }

		internal double TotalSizeInByte()
		{
			if (_content == null)
				return 0;

			var result = _content.ToBytesWithSignature(Encoding.Default).Length;

			result += Scriptsfiles.Sum(x=> x.Value.ToBytesWithSignature(Encoding.Default).Length);
			result += Stylefiles.Sum(x => x.Value.ToBytesWithSignature(Encoding.Default).Length);
			result += Pictures.Sum(x => x.Value.Length);

			return result;
		}

		internal static string GetTotalSizeHumanReadable(double bytes)
		{
			string[] sizes = { "B", "KB", "MB", "GB" };
			var len = bytes;
			var order = 0;
			while (len >= 1024 && order + 1 < sizes.Length)
			{
				order++;
				len = len / 1024;
			}

			return string.Format("{0:0.00} {1}", len, sizes[order]);
		}

        private bool IsPartOfSite(Uri uri)
        {
            return uri.Host.EndsWith(_baseUri.Host.TrimStart("www.")) 
                && uri.AbsolutePath.StartsWith(_baseUri.AbsolutePath);
        }

		private Uri GetUri(string value)
		{
			var uri = value;

			if (uri == null)
				return null;

			if (Uri.IsWellFormedUriString(uri, UriKind.Absolute))
				return new Uri(uri);

			if (Uri.IsWellFormedUriString(uri, UriKind.Relative))
				return new Uri(_baseUri, uri);

			return null;
		}

		private static async Task<byte[]> GetByteFile(Uri uri)
		{
			var client = new WebClient();
			client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

			return await client.DownloadDataTaskAsync(uri.ToString());
		}

		private static async Task<string> GetStringFile(Uri uri)
		{
			var client = new WebClient();
			client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
			return await client.DownloadStringTaskAsync(uri.ToString());
		}
	}
}