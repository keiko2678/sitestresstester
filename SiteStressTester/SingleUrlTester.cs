﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace SiteStressTester
{
    internal class SingleUrlTester
    {
        private readonly Uri _baseUrl;

        private readonly Stack<Uri> _workStack;
        private readonly int _repetitions;

        private readonly DateTime _startTime;
        internal TimeSpan WorkTime;

        private readonly Dictionary<int, TimeSpan> _timeUsed;
        private double _totalDownload;


        internal SingleUrlTester(int numberOfThreads, Uri baseUrl, int repetitions)
        {
            _baseUrl = baseUrl;
            var basePage = new WebPage(baseUrl, _baseUrl);
            basePage.LoadAll().Wait();
            var threads = new List<Thread>();
            _workStack = new Stack<Uri>();
            _timeUsed = new Dictionary<int, TimeSpan>();

            for (var i = 1; i <= numberOfThreads; i++)
            {
                threads.Add(new Thread(() => Worker()));
            }

            var drawerThread = new Thread(() => Drawer());

            _repetitions = repetitions;
            for (var i = 1; i <= repetitions; i++)
            {
                _workStack.Push(basePage.Uri);
            }

            _startTime = LocalTime.Now;
            foreach (var thread in threads)
            {
                thread.Start();
            }
            drawerThread.Start();

            foreach (var thread in threads)
            {
                thread.Join();
            }
            drawerThread.Join();
            WorkTime = LocalTime.Now.Subtract(_startTime);

        }

        private void Worker()
        {
            Uri uri;
            int count;
            WebPage webPage;

            while (true)
            {
                lock (_workStack)
                {
                    if (_workStack.None())
                        break;
                    uri = _workStack.Pop();
                    count = _repetitions - _workStack.Count;
                }
                webPage = new WebPage(uri, _baseUrl);
                webPage.LoadAll().Wait();

                lock (_timeUsed)
                {
                    _timeUsed.Add(count, webPage.LoadTime);
                }

                _totalDownload += webPage.TotalSizeInByte();
            }
        }

        private void Drawer()
        {
            Console.Write("\n\n");
            var count = 0;
            while (true)
            {
                ProgressBar(count, _repetitions);

                lock (_workStack)
                {
                    if (_workStack.None())
                        break;
                    count = _repetitions - _workStack.Count;
                }
                Thread.Sleep(50);
            }
            ProgressBar(_repetitions, _repetitions);
        }

        private void ProgressBar(int progress, int total)
        {
          //  Console.CursorTop -= 1;
            Program.ClearConsoleLine(2);


            //draw empty progress bar
            Console.CursorLeft = 0;
            Console.CursorLeft = 30;
            Console.CursorLeft = 0;
            var onechunk = 29.9f / total;

            //draw filled part
            var position = 1;
            for (var i = 0; i < onechunk * progress; i++)
            {
                Console.BackgroundColor = ConsoleColor.Green;
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            //draw unfilled part
            for (var i = position; i <= 30; i++)
            {
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            //draw totals
            Console.CursorLeft = 35;
            Console.BackgroundColor = ConsoleColor.Black;


            var currentTime = LocalTime.Now.Subtract(_startTime);
            var bitPerSec = _totalDownload / currentTime.TotalSeconds;
            var speed = "";
            var downloaded = "";

            if (_totalDownload > 0)
            {
                speed = " - " + WebPage.GetTotalSizeHumanReadable(bitPerSec) + "\\sec";
                downloaded = " - Total: " + WebPage.GetTotalSizeHumanReadable(_totalDownload);
            }
            Console.Write(progress.ToString() + " of " + total.ToString() + speed + downloaded + "              "); //blanks at the end remove any excess
        }
    }
}
